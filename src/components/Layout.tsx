import { Outlet, NavLink } from 'react-router-dom';
import { Footer } from '../components';

export function Layout() {
  const activeStyle = {
    fontWeight: 'bold',
    textDecoration: 'underline',
    color: 'red',
  };

  return (
    <>
      <nav>
        <NavLink to='/' style={({ isActive }) => (isActive ? activeStyle : {})}>
          Home
        </NavLink>
        <NavLink
          to='about'
          style={({ isActive }) => (isActive ? activeStyle : {})}
        >
          About
        </NavLink>
        <NavLink
          to='contact'
          style={({ isActive }) => (isActive ? activeStyle : {})}
        >
          Contact
        </NavLink>
      </nav>
      <main>
        <Outlet />
      </main>
      <Footer />
    </>
  );
}

export function Home() {
  return (
    <>
      <h2>This is the Home Page</h2>
      <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nesciunt,
        atque praesentium rerum harum consectetur reiciendis cumque asperiores a
        deserunt dignissimos quibusdam dolor sed sapiente nam vero sunt
        cupiditate alias ad?
      </p>

      <p>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nam odit
        quaerat, quibusdam accusantium ipsum molestiae, iure consectetur
        aspernatur, esse porro maxime necessitatibus ducimus neque dolore cumque
        dicta!
      </p>
    </>
  );
}

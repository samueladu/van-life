import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Home, About, Contact } from './pages';
import { Layout } from './components';
import './server';

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout />}>
            <Route index element={<Home />} />
            <Route path='about' element={<About />} />
            <Route path='contact' element={<Contact />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
